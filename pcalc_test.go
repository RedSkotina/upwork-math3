package main

import (
	"math/big"
    "testing"
)
var result int64
func BenchmarkFilter1(b *testing.B) {
        x:=big.NewInt(1e18)
        b1:=big.NewInt(1)
        b7:=big.NewInt(7)
        b8:=big.NewInt(8)
        var t int64 = 0
        for n := 0; n < b.N; n++ {
            if x.Mod(x,b8).Cmp(b1) == 0 && x.Mod(x,b8).Cmp(b7) == 0 {
                t++
            }
        }
        result = t
}

func BenchmarkFilter2(b *testing.B) {
        x:=big.NewInt(1e18)
        g:=big.NewInt(0)
        h:=big.NewInt(0)
        b1:=big.NewInt(1)
        b7:=big.NewInt(7)
        var t int64 = 0
        for n := 0; n < b.N; n++ {
            if g.Sub(x,b1).And(g,b7).BitLen() == 0 && h.Sub(x,b7).And(h,b7).BitLen() == 0 {
                t++
            }
        }
        result = t
}


/*
func pow2b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), uint(b))
	return
}

var pow2 map[int64]*big.Int
func pow2calc(chin <-chan *big.Int, chout chan<- *big.Int) {
	var minkey int64 = 0
	for x := range chin {
		xi := x.Int64()
		v, ok := pow2[xi]
		if ok {
			chout <- v
		} else {
			b := true
			for k, _ := range pow2 {
				if b {
					minkey = k
					b = false
				}
				if k < minkey {
					minkey = k
				}
			}
			delete(pow2, minkey)
			v := big.NewInt(0)
			pow2[xi] = pow2b(v, xi)
			chout <- v
		}
	}
}
func genLookupTable(size int64) {
	pow2 = make(map[int64]*big.Int)
	for i := 0; int64(i) < size; i++ {
		pow2[int64(i)] = pow2b(big.NewInt(0), int64(i))
	}
}
var pow2in chan *big.Int = make(chan *big.Int)
var pow2out chan *big.Int = make(chan *big.Int)
*/

/*
func BenchmarkPow1(b *testing.B) {
    //close(pow2in)
    //close(pow2out)
    genLookupTable(10)
    //go pow2calc(pow2in, pow2out)
    pow2ax := big.NewInt(0)
    var ax int64 = 10e6
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        pow2ax = pow2b(pow2ax,ax)
       
    }
    result = pow2ax.Int64()
}

func BenchmarkPow2(b *testing.B) {
    genLookupTable(10)
    go pow2calc(pow2in, pow2out)
    pow2ax := big.NewInt(0)
    ax := big.NewInt(10e8)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        pow2in <- ax
        select {
        case c := <-pow2out:
            pow2ax = c
        }
    }
    
    result = pow2ax.Int64()
}

func BenchmarkPow4(b *testing.B) {
    genLookupTable(10)
    go pow2calc(pow2in, pow2out)
    pow2ax := big.NewInt(0)
    ax := big.NewInt(10e8)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        pow2in <- ax
        select {
        case c := <-pow2out:
            pow2ax = c
        }
    }
    result = pow2ax.Int64()
}
*/

func BenchmarkPow2b(b *testing.B) {
    ax := big.NewInt(10e5)
    t := big.NewInt(0)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        t = pow2b(t,ax.Int64())
    }
    result = t.Int64()
}
func BenchmarkPow4b(b *testing.B) {
    ax := big.NewInt(10e5)
    t := big.NewInt(0)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        t = pow4b(t,ax.Int64())
    }
    result = t.Int64()
}
func BenchmarkPow4c(b *testing.B) {
    ax := big.NewInt(10e5)
    t := big.NewInt(0)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        t = pow2b(t,ax.Int64())
        t = t.Mul(t,t)
    }
    result = t.Int64()
}
func BenchmarkExp2(b *testing.B) {
    ax := big.NewInt(10e5)
    t := big.NewInt(0)
    b2 := big.NewInt(2)
    b.ResetTimer()
    for n := 0; n < b.N; n++ {
        t = t.Exp(b2,ax,nil)
    }
    result = t.Int64()
}
