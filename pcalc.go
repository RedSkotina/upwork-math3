package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math/big"
	"os"
	"runtime"
	"runtime/pprof"
	"sync"
	"time"
)

var (
	Info     *log.Logger
	Error    *log.Logger
	NumCores int
)

func Init(
	infoHandle io.Writer,
	errorHandle io.Writer) {

	Info = log.New(infoHandle,
		"INFO: ", 0)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

type Result struct {
	p0   *big.Int
	px   *big.Int
	time time.Duration
}

func (r Result) toString() string {
	var s string
	s = fmt.Sprintf("p0=%d px=%d time=%s\n", r.p0, r.px, r.time)
	return s
}
func (r Result) toStringEx() string {
	var s string
	s = fmt.Sprintf("p0=%d px=%d time=%s\n", r.p0, r.px, r.time)
	return s
}

type IterParam struct {
	a0 *big.Int
	n0 *big.Int
	p0 *big.Int
	k0 *big.Int
	x  *big.Int
    a0even bool
}
type IterBatchParam struct {
	arr []*IterParam
}

type IterResult struct {
	modis1 bool
	px     *big.Int
    ax     *big.Int
    nx     *big.Int
	x      *big.Int
}

/*
func log2(x *big.Int) int64 {
	return int64(x.BitLen() - 1)
}
*/
// WARNING: check outbounds
func log2(r *big.Int, x *big.Int) *big.Int {
	return r.SetInt64(int64(x.BitLen() - 1))
}

// warning: only for positive integer powers of 2
func pow2b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), uint(b))
	return
}
func pow4b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), 2*uint(b))
	return
}

type Cache struct {
	pow2m map[int64]*big.Int
    pow4m map[int64]*big.Int
}

func (r *Cache) genLookupTables(size int64) {
    r.pow2m = make(map[int64]*big.Int)
    for i := 0; int64(i) < size; i++ {
		r.pow2m[int64(i)] = pow2b(big.NewInt(0), int64(i))
	}
    r.pow4m = make(map[int64]*big.Int)
	for i := 0; int64(i) < size; i++ {
		r.pow4m[int64(i)] = pow4b(big.NewInt(0), int64(i))
	}  
}

func (r *Cache) pow2(x *big.Int) *big.Int{
    var minkey int64 = 0
    xi := x.Int64()
    v, ok := r.pow2m[xi]
    if ok {
        return v
    } else {
        b := true
        for k,_ := range r.pow2m {
            if (b) {
                minkey = k
                b = false
            }
            if k<minkey {
                minkey = k
            }
        }
        delete(r.pow2m,minkey)
        v := big.NewInt(0)
        r.pow2m[xi] = pow2b(v,xi)
        return v
    }
}

func (r *Cache) pow4(x *big.Int) *big.Int{
    var minkey int64 = 0
    xi := x.Int64()
    v, ok := r.pow4m[xi]
    if ok {
        return v
    } else {
        b := true
        for k,_ := range r.pow4m {
            if (b) {
                minkey = k
                b = false
            }
            if k<minkey {
                minkey = k
            }
        }
        delete(r.pow4m,minkey)
        v := big.NewInt(0)
        r.pow4m[xi] = pow4b(v,xi)
        return v
    }
}

/*
(A) a0= int(log2(p0)), k0=p0-2^a0, n0= if a is odd then (p0-a0)/2, if a is even, then (p0-a0+1)/2.
(B) goto (E)
(C) px (p index number x) =2*x*p0+1, where x=1,2,3..... Take x=1 and calculate px.
(D) ax= int(log2(px)), kx=px-2^ax,
nx=n0-((ax-a0)/2) if ax is even and a0 is even OR if ax is odd and a0 is odd
nx=n0-((ax-a0+1)/2) if ax is odd and a0 is even
nx=n0-((ax-a0-1)/2) if ax is even and a0 is odd
(E) calculate formula "F" defined in (F)
(F) mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx+1)-1)/3*kx-1 if ax is odd
mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx)-1)/3*kx-1 if ax is even
(G) rx= mx mod(px)
(H) if rx=1 goto (S)
(I) if rx not 1 goto C and follow same order with x=x+1
(S) output: p0, px
*/

func ipxCalc0(pch0 <-chan IterBatchParam, iresults chan<- IterResult, iwg *sync.WaitGroup,cache *Cache ) {
	defer iwg.Done()
	// constants
	var b0 = big.NewInt(0)
	var b1 = big.NewInt(1)
	var b2 = big.NewInt(2)
	var b3 = big.NewInt(3)
	var b4 = big.NewInt(4)
    //var b7 = big.NewInt(7)
	//var b8 = big.NewInt(8)
    // temporary variables for save result of func Add, Sub, Div, Mul, Mod
	var t = big.NewInt(0)
    var t1 = big.NewInt(0)
	// cache variables
	var mxe1 = big.NewInt(0)
	var mxe2 = big.NewInt(0)
	var pow2ax = big.NewInt(0)
    var pow4nx = big.NewInt(0)
	//var p0x2 = big.NewInt(0)
	// iteration variables
	var px = big.NewInt(0)
	var ax = big.NewInt(0)
    var nx = big.NewInt(0)
    var kx = big.NewInt(0)
	var mx = big.NewInt(0)
	var rx = big.NewInt(0)

	for ibp := range pch0 {
		res := new(IterResult)
		for _, ip := range ibp.arr {
			res.x = ip.x
            px.Set(ip.p0)
            ax = ip.a0
            kx = ip.k0
            nx = ip.n0
            pow2ax = cache.pow2(ax)
            //pow2ax = pow2t[ax.Int64()]
            // F: Formula
			if t.Mod(ax,b2).BitLen() == 0 {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Add(kx, b0)
			} else {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx+1)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-4*kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Mul(b4, kx)
			}
            
            pow4nx = cache.pow4(nx)
			mxe2 = mxe2.Add(mxe2.Add(mxe2.Mul(pow4nx, mxe2.Sub(mxe2.Mul(b2, pow2ax), mxe1)), pow2ax), kx)
			mx = mx.Sub(mx.Div(mxe2, b3), b1)

			rx = t1.Mod(mx, px)

			if rx.Cmp(b1) == 0 {
				res.modis1 = true
				break
			}
            //Info.Println("x", ip.x,"px",px, "ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx)
				
		}
        res.px = px
		iresults <- *res
	}
}

func ipxCalc(pch <-chan IterBatchParam, iresults chan<- IterResult, iwg *sync.WaitGroup, cache *Cache) {
	defer iwg.Done()
	// constants
	var b0 = big.NewInt(0)
	var b1 = big.NewInt(1)
	var b2 = big.NewInt(2)
	var b3 = big.NewInt(3)
	var b4 = big.NewInt(4)
    var b7 = big.NewInt(7)
	var b8 = big.NewInt(8)
    // temporary variables for save result of func Add, Sub, Div, Mul, Mod
	var t = big.NewInt(0)
	var t0 = big.NewInt(0)
	var t1 = big.NewInt(0)
	// cache variables
    var axeven bool
	var mxe1 = big.NewInt(0)
	var mxe2 = big.NewInt(0)
	var pow2ax = big.NewInt(0)
	var pow4nx = big.NewInt(0)
    //var p0x2 = big.NewInt(0)
	// iteration variables
	var px = big.NewInt(0)
	var ax = big.NewInt(0)
    var nx = big.NewInt(0)
	var kx = big.NewInt(0)
	var mx = big.NewInt(0)
	var rx = big.NewInt(0)

	for ibp := range pch {
        res := new(IterResult)
		for _, ip := range ibp.arr {
            res.x = ip.x
            px = px.Add(px.Mul( px.Mul(ip.p0,b2), ip.x), b1) 
            if (t0.Sub(px,b1).Mod(t0,b8).BitLen() != 0) && (t0.Sub(px,b7).Mod(t0,b8).BitLen() != 0) { 
                continue
            }
            ax = log2(ax,px)
            pow2ax = cache.pow2(ax)
            //pow2ax = pow2t[ax.Int64()]
            kx = kx.Sub(px, pow2ax)
            //Info.Println("x", ip.x,"px",px, "ax",ax,"kx",kx,"a0",ip.a0,"n0",ip.n0,"p0",ip.p0)
            axeven = (t.Mod(ax,b2).BitLen() == 0)
            switch {
            case ( axeven && ip.a0even) || (!axeven && !ip.a0even):
                {
                    nx = nx.Sub(ip.n0,nx.Div(nx.Sub(ax,ip.a0),b2))
                }
            case (!axeven && ip.a0even):
                {
                    nx = nx.Sub(ip.n0,nx.Div(nx.Add(nx.Sub(ax,ip.a0),b1),b2))
                }
            case (axeven && !ip.a0even):
                {
                    nx = nx.Sub(ip.n0,nx.Div(nx.Sub(nx.Sub(ax,ip.a0),b1),b2))
                }
            default:
                Error.Println("nx wrong case. IMPOSSIBLE!!!!!")
			}
			// F: Formula
			if axeven {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Add(kx, b0)
			} else {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx+1)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-4*kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Mul(b4, kx)
			}
            pow4nx = cache.pow4(nx)
            mxe2 = mxe2.Add(mxe2.Add(mxe2.Mul(pow4nx, mxe2.Sub(mxe2.Mul(b2, pow2ax), mxe1)), pow2ax), kx)
			mx = mx.Sub(mx.Div(mxe2, b3), b1)

			rx = t1.Mod(mx, px)

			if rx.Cmp(b1) == 0  {
				res.modis1 = true
				break
			}
            //Info.Println("x", ip.x,"px",px, "ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx)
			res.ax = ax
            res.nx = nx
        }
        res.px = px
        
        iresults <- *res
	}
}

func pxCalc(jobs <-chan *big.Int, results chan<- Result, wg *sync.WaitGroup) {
	defer wg.Done()
	for p0 := range jobs {
		start := time.Now()
		Info.Println("start calculation with p0 =", p0.String())
		res := new(Result)
		res.p0 = p0

		pch0 := make(chan IterBatchParam, 1)
        pch := make(chan IterBatchParam)
        iresults := make(chan IterResult)
		done := make(chan bool)
		iwg := new(sync.WaitGroup)
        
        maxsize := 100
        Info.Println("pow2 table initial size =" ,maxsize)
        cache := make([]*Cache,NumCores+1)
		for w := 0; w <= NumCores; w++ {
            cache[w] = new(Cache)
            maxsize := 100
            cache[w].genLookupTables(int64(maxsize))
            
        }
        // start worker for x = 0
		iwg.Add(1)
		go ipxCalc0(pch0, iresults, iwg, cache[0])
		
        // start other workers
		for w := 1; w <= NumCores; w++ {
			iwg.Add(1)
			go ipxCalc(pch, iresults, iwg, cache[w])
		}
		// send input values
		go func() {
            var a0even bool
            var pow2a0 = big.NewInt(0)
            var a0 = big.NewInt(0)
            var n0 = big.NewInt(0)
			var k0 = big.NewInt(0)
			var b1 = big.NewInt(1)
			var b2 = big.NewInt(2)
            var t = big.NewInt(0)
            x := big.NewInt(0)
			for {
				select {
				case <-done:
					close(pch)
                    return
				default:
                    switch {
                    case x.BitLen() == 0:
                        a0 = log2(a0,p0)
                        pow2a0 = pow2b(pow2a0, a0.Int64())
                        k0 = k0.Sub(p0, pow2a0)
                        n0 = n0.Div(n0.Sub(p0,a0),b2)
                        a0even = (t.Mod(a0,b2).BitLen() == 0)
                        if a0even {
                            n0 = n0.Div(n0.Add(n0.Sub(p0,a0),b1),b2)
                        }
                        
                        ibp := new(IterBatchParam)
                        ibp.arr = make([]*IterParam, 1)
                        ip := &IterParam{x:big.NewInt(0),
                                        a0:a0, p0:p0, k0:k0, n0:n0, a0even: a0even}
                        ibp.arr[0] = ip
                        x.Add(x, b1)
                        pch0 <- *ibp
                        close(pch0) // we wait only one value
                    default:
                        ibp := new(IterBatchParam)
                        ibp.arr = make([]*IterParam, *batch)
                        var i int64
                        for i = 0; i < *batch; i++ {
                            ip := &IterParam{x:big.NewInt(0).Set(x),
                                a0:a0, p0:p0,  k0:k0, n0:n0, a0even: a0even}
                            ibp.arr[i] = ip
                            x.Add(x, b1)
                        }
                        pch <- *ibp
                    }                    
				}
			}
		}()
		// Collect all results
		go func() {
			iwg.Wait()
			close(iresults)
		}()
        
        tgp_timer := time.Now()
		btrace := big.NewInt(*trace)
        xb := big.NewInt(0)
		t := big.NewInt(0)
        b1 := big.NewInt(1)
		
		done_closed := false
		for r := range iresults {
            xb.Add(xb,b1)
			//Info.Println(gx.Int64())
			if *trace != 0 && (t.Mod(t.Sub(xb,b1),btrace).BitLen() == 0) {
				Info.Println("x", r.x, "px", r.px, "ax", r.ax, "nx", r.nx) // ,"ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx
				tgp_time := time.Since(tgp_timer)
				// windows timer have 15 ms precision only
				// force 0ms to 15ms
				if tgp_time.Nanoseconds() == 0 {
					tgp_time = time.Duration(15) * time.Millisecond
				}
                // 0nth series contain only 1 x ( so general count of x less by (*batch)-1 )
                Info.Println("throughput x/sec = ", (((*batch)*(*trace) )*1e9)/(tgp_time.Nanoseconds()))
				tgp_timer = time.Now()
            }
			if r.modis1 {
				if !done_closed {
					close(done)
                    done_closed = true
				}
				
				res.px = r.px
				res.time = time.Since(start)
				results <- *res
			}
			
		}
	}
}

//var pow2in chan *big.Int= make(chan *big.Int)
//var pow2out chan *big.Int= make(chan *big.Int)
//var pow4in chan *big.Int= make(chan *big.Int)
//var pow4out chan *big.Int= make(chan *big.Int)
       
func processFile(infile string, outfile string) {
	file, err := os.Open(infile)
	if err != nil {
		Error.Println("Could not open file ", infile)
		return
	}
	defer file.Close()

	// unbuffered channels
	jobs := make(chan *big.Int)
	results := make(chan Result)
	// wait group
	wg := new(sync.WaitGroup)
	// start workers
	for w := 1; w <= NumCores; w++ {
		wg.Add(1)
		go pxCalc(jobs, results, wg)
	}

	// scan each line of file and queue up p-number
	go func() {
        defer close(jobs)
        var p0sl[]*big.Int
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
            p0 := big.NewInt(0)
            p0, suc := p0.SetString(scanner.Text(),10)
            if !suc {
                Error.Println("Cant convert '", scanner.Text(), "' to big.Int with base 10")
                continue
            }
			p0sl = append(p0sl,p0)
		}
        if len(p0sl) == 0 {
            return
        }
        p0max := p0sl[0]
        for _,p0 := range p0sl {
            if p0.Cmp(p0max) > 0 {
                p0max = p0
            }
        }
        //maxsize := detMaxPow2(p0max)
        for _,p0 := range p0sl {
            jobs <- p0
        }
	}()

	// Collect all results
	go func() {
		wg.Wait()
		//close(done)
		close(results)
	}()

    //go pow2calc(pow2in,pow2out)
    //go pow4calc(pow4in,pow4out)
    
	wch := make(chan Result)
	// output results to file
	go func(wch <-chan Result) {
		wpath := outfile
		wfile, err := os.Create(wpath)
		if err != nil {
			Error.Println("Could not open file ", wpath)
			return
		}
		defer wfile.Close()
		w := bufio.NewWriter(wfile)
		for v := range wch {
			_, err := w.WriteString(v.toStringEx())
			if err != nil {
				Error.Println("Could not write to file ", wpath)
				return
			}
			w.Flush()
		}
	}(wch)

	// send all results from the results channel to the output channel
	for v := range results {
		fmt.Print(v.toString())
		wch <- v
	}
	close(wch)
}

var batch = flag.Int64("batch", 100, "set number of x per series(goroutine)")
var trace = flag.Int64("trace", 100000, "trace results to stdout every N series, exclude step p0")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
//var ptimeout   = flag.Duration("timeout", time.Duration(24)* time.Hour, "timeout for calculation p")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Start cpu profile")
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	//skip := ioutil.Discard
	Init(os.Stdout, os.Stderr)

	if len(flag.Args()) < 1 {
		fmt.Println("Usage: gopcalc [flags] inputfile")
		fmt.Println("flags:")
		flag.PrintDefaults()
		os.Exit(1)
	}
	var infile string = flag.Args()[0]

	NumCores = runtime.GOMAXPROCS(-1)
	Info.Println("The number of logical CPUs usable by the current process =", runtime.NumCPU())
	Info.Println("Maximum number of CPUs that can be executing simultaneously =", runtime.GOMAXPROCS(-1))
	Info.Println("Iterations per goroutine = ", *batch)
	//Info.Println(fmt.Sprintf("Timeout for goroutine = %s", ptimeout)   )
    	
	start := time.Now()
	processFile(infile, "./out.txt")
	elapsed := time.Since(start)
	Info.Println("Processing took ", elapsed)
}
