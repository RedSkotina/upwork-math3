#!/usr/bin/env python3
import sys,os
import timeit
import io
import time
from functools import wraps, partial, reduce
import math 
class Result:
    def __init__(self):
        self.P = -1
        self.PX = -1
    def toString(self):
        s = ""
        s = "p={} px={}\n".format(self.P, self.PX)
        return s

    def toStringEx(self):
        s = ""
        s = "p={} px={}\n".format(self.P, self.PX)
        return s

def coroutine(func_gen):
    @wraps(func_gen)
    def starter(*args, **kwargs):
        cr = func_gen(*args, **kwargs)
        if cr is None:
            return None
        _ = next(cr)
        return cr
    return starter

@coroutine
def read_chunk(file_object, chunksize, target):
    """
    read enless stream with a .read method
    """
    for line in file_object:
        target.send(line)

@coroutine
def process_chunk(target):
    def p_calculate(j):
        #start = time.time()
        #Info.Println("p",j)
        res = Result()
        
        p = int(j)
        res.P = p 
        
        p0 = p
        x = 0
        a0 = int(math.log2(p0)) 
        k0 = p0-2**a0           
        n0 =  (p0-a0+1)//2 if a0%2 == 0 else (p0-a0)//2 
        rx = 0

        px = 0
        while rx != 1:
            px = 2*x*p0+1
            ax = int(math.log2(px)) 
            kx = px-2**ax           
            nx = 0
            if (ax%2 == 0 and a0%2 == 0) or (ax%2 != 0 and a0%2 != 0):     
                nx = n0-((ax-a0)/2)   
            elif (ax%2 != 0 and a0%2 == 0):
                nx = n0-((ax-a0+1)/2)
            elif (ax%2 == 0 and a0%2 != 0):
                nx = n0-((ax-a0-1)/2)
                
            if x == 0:
                px = p0
                ax = a0
                kx = k0
                nx = n0
            mx = 0
            nx = int(nx)
            if ax%2 == 0:    
                #mx = (2**(2*nx+1)+1)/3*2**ax-(4**(nx)-1)/3*kx-1
                
                mx10 = 2**(2*nx+1) + 1
                mx11 = mx10//3
                mx12 = 2**ax
                mx1 = mx11*mx12
                mx2 = (4**(nx)-1)//3*kx
                mx = mx1 - mx2 -1
            else:
                mx = (2**(2*nx+1)+1)//3*2**ax-(4**(nx+1)-1)//3*kx-1
            mx = int(mx)
            px = int(px)
            rx = mx % px
            #print("x",x,"px",px,"ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx)
            x = x + 1
        res.PX = px
        #end = time.time()
        #print("x took {}".format( end - start))
        return res
    while True:
        chunk = (yield)
        data  = p_calculate(chunk)
        print(data.toString(), end='')
        target.send(data)
        
@coroutine
def write_chunk(file_object):
    while True:
        writable = (yield)
        file_object.write(writable.toStringEx().encode())
        file_object.flush()



def processFile(src, dst): 
    r = open(src, 'rb')
    w = open(dst, 'wb')

    g = reduce(lambda a, b: b(a),
               [w, write_chunk, process_chunk,
                partial(read_chunk, r, 16)]
              )
    v = 0
    while g is not None:
        v = next(g)
    

def main():
    start = time.time()
    processFile("sources.txt","out2.txt")
    end = time.time()
    print("Processing took {} sec".format( end - start))

main()
