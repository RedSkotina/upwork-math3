package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math/big"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
	"sync"
	"time"
)

var (
	Info     *log.Logger
	Error    *log.Logger
	NumCores int
)

func Init(
	infoHandle io.Writer,
	errorHandle io.Writer) {

	Info = log.New(infoHandle,
		"INFO: ", 0)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

type Result struct {
	p0   int64
	px   int64
	time time.Duration
}

func (r Result) toString() string {
	var s string
	s = fmt.Sprintf("p0=%d px=%d time=%s\n", r.p0, r.px, r.time)
	return s
}
func (r Result) toStringEx() string {
	var s string
	s = fmt.Sprintf("p0=%d px=%d time=%s\n", r.p0, r.px, r.time)
	return s
}

type IterParam struct {
	a0 int64
	n0 int64
	p0 int64
	k0 *big.Int
	x  *big.Int
}
type IterBatchParam struct {
	arr []*IterParam
}

type IterResult struct {
	modis1 bool
	px     int64
	x      *big.Int
}

func log2(x *big.Int) int64 {
	return int64(x.BitLen() - 1)
}

// warning: only for positive integer powers of 2
func pow2b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), uint(b))
	return
}

var pow2t [1024]*big.Int

func genPow2LookupTable() {
	for i, _ := range pow2t {
		pow2t[i] = pow2b(big.NewInt(0), int64(i))
	}
}

/*
(A) a0= int(log2(p0)), k0=p0-2^a0, n0= if a is odd then (p0-a0)/2, if a is even, then (p0-a0+1)/2.
(B) goto (E)
(C) px (p index number x) =2*x*p0+1, where x=1,2,3..... Take x=1 and calculate px.
(D) ax= int(log2(px)), kx=px-2^ax,
nx=n0-((ax-a0)/2) if ax is even and a0 is even OR if ax is odd and a0 is odd
nx=n0-((ax-a0+1)/2) if ax is odd and a0 is even
nx=n0-((ax-a0-1)/2) if ax is even and a0 is odd
(E) calculate formula "F" defined in (F)
(F) mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx+1)-1)/3*kx-1 if ax is odd
mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx)-1)/3*kx-1 if ax is even
(G) rx= mx mod(px)
(H) if rx=1 goto (S)
(I) if rx not 1 goto C and follow same order with x=x+1
(S) output: p0, px
*/

func ipxCalc(xch <-chan IterBatchParam, iresults chan<- IterResult, iwg *sync.WaitGroup) {
	defer iwg.Done()
	// constants
	var b0 = big.NewInt(0)
	var b1 = big.NewInt(1)
	var b2 = big.NewInt(2)
	var b3 = big.NewInt(3)
	var b4 = big.NewInt(4)
    var b7 = big.NewInt(7)
	var b8 = big.NewInt(8)
    // temporary variables for save result of func Add, Sub, Div, Mul, Mod
	var t0 = big.NewInt(0)
	var t1 = big.NewInt(0)
	// cache variables
	var mxe1 = big.NewInt(0)
	var mxe2 = big.NewInt(0)
	var pow2ax = big.NewInt(0)
	//var p0x2 = big.NewInt(0)
	// iteration variables
	var ax, nx int64
	var px = big.NewInt(0)
	var kx = big.NewInt(0)
	var mx = big.NewInt(0)
	var rx int64 = 0

	for ibp := range xch {
		res := new(IterResult)
		for _, ip := range ibp.arr {
			res.x = ip.x
			if ip.x.BitLen() == 0 { // x == 0?
				px = px.SetInt64(ip.p0)
				ax = ip.a0
				nx = ip.n0
				kx = ip.k0
				pow2ax = pow2t[ax]
			} else {
				px = px.Add(px.Mul(big.NewInt(2*ip.p0), ip.x), b1)
                if (t0.Sub(px,b1).Mod(t0,b8).BitLen() != 0) && (t0.Sub(px,b7).Mod(t0,b8).BitLen() != 0) {
                    continue
                }
				ax = log2(px)
				pow2ax = pow2t[ax]
				kx = kx.Sub(px, pow2ax)
				switch {
				case (ax%2 == 0 && ip.a0%2 == 0) || (ax%2 != 0 && ip.a0%2 != 0):
					{
						nx = ip.n0 - ((ax - ip.a0) / 2)
					}
				case (ax%2 != 0 && ip.a0%2 == 0):
					{
						nx = ip.n0 - ((ax - ip.a0 + 1) / 2)
					}
				case (ax%2 == 0 && ip.a0%2 != 0):
					{
						nx = ip.n0 - ((ax - ip.a0 - 1) / 2)
					}
				default:
					Error.Println("wrong case")
				}
			}
			// F: Formula
			if ax%2 == 0 {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Add(kx, b0)
			} else {
				// mx=(2^(2*nx+1)+1)/3*2^ax-(4^(nx+1)-1)/3*kx-1
				// ->
				// mx=(2^(2nx)*(2*2^ax-4*kx)+2^ax+kx)/3-1
				mxe1 = mxe1.Mul(b4, kx)
			}
			mxe2 = mxe2.Add(mxe2.Add(mxe2.Mul(pow2t[2*nx], mxe2.Sub(mxe2.Mul(b2, pow2ax), mxe1)), pow2ax), kx)
			mx = mx.Sub(mx.Div(mxe2, b3), b1)

			rx = t1.Mod(mx, px).Int64()
            Info.Println("x", ip.x,"px",px, "ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx)
			if rx == 1 {
				res.modis1 = true
				break
			}
		}
		res.px = px.Int64()
		iresults <- *res
	}
}

func pxCalc(jobs <-chan string, results chan<- Result, wg *sync.WaitGroup) {
	defer wg.Done()
	for s := range jobs {
		start := time.Now()
		Info.Println("start calculation with p0 =", s)
		res := new(Result)

		p0, err := strconv.ParseInt(s, 10, 0)
		if err != nil {
			Error.Println("Cant convert", s, "to int")
			results <- *res
			break
		}
		res.p0 = p0

		xch := make(chan IterBatchParam)
		iresults := make(chan IterResult)
		done := make(chan bool)
		iwg := new(sync.WaitGroup)

		// start workers
		for w := 1; w <= NumCores; w++ {
			iwg.Add(1)
			go ipxCalc(xch, iresults, iwg)
		}
		tgp_timer := time.Now()
		// send input values
		go func() {
			var t0 = big.NewInt(0)
			a0 := log2(big.NewInt(p0))
			k0 := t0.Sub(big.NewInt(p0), pow2t[a0])
			n0 := (p0 - a0) / 2
			if a0%2 == 0 {
				n0 = (p0 - a0 + 1) / 2
			}
			//p0x2.SetInt64(2*p0)
			var b1 = big.NewInt(1)
			x := big.NewInt(0)
			for {
				select {
				case <-done:
					close(xch)
					return
				default:
					ibp := new(IterBatchParam)
					ibp.arr = make([]*IterParam, *batch)
					var i int64
					for i = 0; i < *batch; i++ {
						ip := &IterParam{x:big.NewInt(0).Set(x),
                            k0:big.NewInt(0).Set(k0),
                            a0:a0, p0:p0, n0:n0}
						ibp.arr[i] = ip
						x.Add(x, b1)
					}
					xch <- *ibp
				}
			}
		}()
		// Collect all results
		go func() {
			iwg.Wait()
			close(iresults)
		}()

		done_closed := false
		for r := range iresults {
			//Info.Println(r.x.Int64(),*trace,r.x.Int64()+*batch)
			if *trace != 0 && (r.x.Int64()%*trace == 0 || r.x.Int64() / *trace < (r.x.Int64()+*batch) / *trace) && (r.x.Int64() != 0 || *trace == 1) {
				Info.Println("x", r.x, "px", r.px) // ,"ax",ax,"kx",kx,"nx",nx,"mx",mx,"rx",rx
				tgp_time := time.Since(tgp_timer)
				// windows timer have 15 ms precision only
				// force 0ms to 15ms
				if tgp_time.Nanoseconds() == 0 {
					tgp_time = time.Duration(15) * time.Millisecond
				}
				Info.Println("throughput iteration/sec", (*trace*1e9)/(tgp_time.Nanoseconds()))
				tgp_timer = time.Now()
			}
			if r.modis1 {
				if !done_closed {
					close(done)
				}
				done_closed = true
				res.px = r.px
				res.time = time.Since(start)
				results <- *res
			}
		}
	}
}

func processFile(infile string, outfile string) {
	file, err := os.Open(infile)
	if err != nil {
		Error.Println("Could not open file ", infile)
		return
	}
	defer file.Close()

	// unbuffered channels
	jobs := make(chan string)
	results := make(chan Result)
	// wait group
	wg := new(sync.WaitGroup)
	// start workers
	for w := 1; w <= NumCores; w++ {
		wg.Add(1)
		go pxCalc(jobs, results, wg)
	}

	// scan each line of file and queue up p-number
	go func() {
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			jobs <- scanner.Text()
		}
		close(jobs)
	}()

	// Collect all results
	go func() {
		wg.Wait()
		//close(done)
		close(results)
	}()

	wch := make(chan Result)
	// output results to file
	go func(wch <-chan Result) {
		wpath := outfile
		wfile, err := os.Create(wpath)
		if err != nil {
			Error.Println("Could not open file ", wpath)
			return
		}
		defer wfile.Close()
		w := bufio.NewWriter(wfile)
		for v := range wch {
			_, err := w.WriteString(v.toStringEx())
			if err != nil {
				Error.Println("Could not write to file ", wpath)
				return
			}
			w.Flush()
		}
	}(wch)

	// send all results from the results channel to the output channel
	for v := range results {
		fmt.Print(v.toString())
		wch <- v
	}
	close(wch)
}

var batch = flag.Int64("batch", 100, "set number iterations per goroutine")
var trace = flag.Int64("trace", 10000000, "trace results to stdout every N iterations")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
//var ptimeout   = flag.Duration("timeout", time.Duration(24)* time.Hour, "timeout for calculation p")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Start cpu profile")
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	//skip := ioutil.Discard
	Init(os.Stdout, os.Stderr)

	if len(flag.Args()) < 1 {
		fmt.Println("Usage: gopcalc [flags] inputfile")
		fmt.Println("flags:")
		flag.PrintDefaults()
		os.Exit(1)
	}
	var infile string = flag.Args()[0]

	NumCores = runtime.GOMAXPROCS(-1)
	Info.Println("The number of logical CPUs usable by the current process =", runtime.NumCPU())
	Info.Println("Maximum number of CPUs that can be executing simultaneously =", runtime.GOMAXPROCS(-1))
	Info.Println("Iterations per goroutine = ", *batch)
	//Info.Println(fmt.Sprintf("Timeout for goroutine = %s", ptimeout)   )

	genPow2LookupTable()
	start := time.Now()
	processFile(infile, "./out.txt")
	elapsed := time.Since(start)
	Info.Println("Processing took ", elapsed)
}
